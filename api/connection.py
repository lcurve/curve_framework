from py2neo import Graph, Node, Relationship
import py2neo
import time
import os

url = os.environ.get('GRAPHENEDB_URL', 'bolt://db:7687')
username = os.environ.get('NEO4J_USERNAME')
password = os.environ.get('NEO4J_PASSWORD')

counter = 0

while counter < 100:
    try:            
        graph = Graph(url + '/db/data/', auth=(username, password))  
        break 
    except py2neo.errors.ConnectionUnavailable as error:
        time.sleep(1)
        counter += 1

if counter == 100:
    raise py2neo.errors.ConnectionUnavailable('A connection to neo4j could not be established, tried {} times.'.format(counter))
