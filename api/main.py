from typing import Optional
from fastapi import FastAPI
from models import Node

app = FastAPI()

@app.get("/")
def read_root():
    return {"hello":"world"}


@app.get("/test")
def test():
    return Node().get_all()