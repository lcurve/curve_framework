from py2neo import Graph, Node, Relationship
from connection import graph

class Node:
    def __init__(self):
        pass

    def get_all(self):
        query = '''
        match (p:Person {healthstatus:"Sick"})
        return p
        limit 1;
        '''

        return (graph.run(query)).data()